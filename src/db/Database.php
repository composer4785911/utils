<?php

namespace Mapeq\DB;

use Closure;
use PDO;
use Exception;
use Mapeq\Logger\Watch;
use Monolog\Level;

class Database {

	private ?PDO $con = null;
	private $url;
	private $password;
	private $user;
	private Watch $watch;
	private $sequence;
	
	function __construct(){

		try {		
			$this->url = getConfig('connection');
			$this->password = getConfig('passwort');
			$this->user = getConfig('username');
			$this->sequence = getConfig('sequence');

		} catch (\Throwable $th) {
			getLogger()->error($th);
		}	
	}
	
	private function connect (){
		$watch = new Watch(Level::Debug);

		$watch->begin();
	
		try {
			if(!isset($this->con) || $this->con == null){

				$param = array(PDO::ATTR_PERSISTENT => true, PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8");
				$this->con = new PDO($this->url, $this->user, $this->password 
				, $param
				);
				$this->con->query("SET NAMES 'utf8'");
				getLogger()->debug("Connected to $this->url");

			}
				
		}catch (Exception $e){
			getLogger()->error($e);
			return FALSE;
		}
	
		$watch->stop("connect DB");
		return true;
	
	}

	public function close(){

		if(isset($this->con)){

			try {
				$this->con->rollBack();
			} catch (\Throwable $th) {}

			$this->con = null;
			
		}


	}
	
	public function dump($file , $database){
		$command = "mysqldump  -u$this->user -p$this->password $database > $file";
		
		shell_exec($command);	
	}
	
	
	public function executeScript ($arSql){
		
		try {
		
			if ( $this->connect() ){
				
				$this->con->beginTransaction();
				$this->con->query("SET NAMES 'utf8'");
				foreach ($arSql as $sql){
					
					$this->con->exec($sql);
				}
					
				$this->con->commit();
			}
		
		
		}catch (Exception $e){
			getLogger()->error($e);

		}
		
		
	}

	public function getCon():PDO{
		$this->connect();
		return $this->con;
	}


	public function transaction(Closure $fnc){

		if ( $this->connect() ){
			$this->con->beginTransaction();

			try {
				$fnc($this->con);
			} catch (\Throwable $th) {
				getLogger()->error($th);
			}finally{
				$this->con->rollBack();
			}
		
		}
	}


	private function logStart($querry, mixed... $param){
		if(getConfig('SQL_LOGGER') == 'TRUE'){ 
			$this->watch = new Watch(Level::Debug);
			if(count($param) > 0){
				$p =  "\n with param: " . implode($param);
			}

			$this->watch->begin($querry . $p);
		}
	}
	
	private function logEnd($rows = null){
		if(getConfig('SQL_LOGGER') === 'TRUE'){ 
			$msg = isset($rows) ? "$rows  rows " : null;
			$this->watch->stop($msg);
		}
	}

	
	public function  simpleQueryFetch($strSql, $fetchMode){
	
		$arResult = null;
	
		try {
				
			if ( $this->connect() ){
				$this->con->query("SET NAMES 'utf8'");

				$this->logStart($strSql);
				$stmResult =  $this->con->query($strSql);
				$this->logEnd();

				if($stmResult != null) {
					$result = $stmResult->setFetchMode($fetchMode);
					$arResult = $stmResult->fetchAll();
				}
					
				$stmResult->closeCursor();
			}
				
		}catch (Exception $e){
			getLogger()->error($e);
		}
		
		return $arResult;
	}
	
	public function  simpleQuery($strSql){
		
		return $this->simpleQueryFetch($strSql, PDO::FETCH_ASSOC);
	}
	
	
	public function preparedInsert($statement,mixed... $params){
		
		if ( $this->connect() ){
			$transaction = !$this->con->inTransaction();
			if($transaction){ 
				$this->con->beginTransaction();
			}
			$this->con->query("SET NAMES 'utf8';");
		      $stm = $this->con->prepare($statement);
		      
			  $this->logStart($statement, ...$params);
		      if($stm->execute ($params)) {
		      	$this->logEnd( $stm->rowCount());
				$ok = $this->lastInsertedId();		 
		      }
			  if($transaction){ 
		     	$this->con->commit();
			  }
		      $stm->closeCursor();
		}
		
		return $ok;
	}

	private function lastInsertedId() : int{
		$id = -1;
		try {
			if(isset($this->sequence)){ 
				$t = $this->con->query( "select lastval(`$this->sequence`) id;", PDO::FETCH_ASSOC)->fetchAll();
				$id = isset( $t[0]['id'] ) ? $t[0]['id'] : 0;
			}else{
				$this->con->lastInsertId();
			}
		} catch (\Throwable $th) {
			getLogger()->error($th);
		}
		return $id;
	}
	
	public function  preparedQuerySingleResult($strSql, mixed... $params){
			$ar = null;
			$arResult = $this->preparedQuery($strSql, ...$params);
			if(isset($arResult) && count($arResult) > 0 ){
				if(count( $arResult) > 1 ){
					throw new Exception("Single result query returns mor than one row");
				}

				if(count($arResult) == 1){
					$ar = $arResult[0];
				} 
			} 
		return $ar;
	
	}

	public function  preparedQuery($strSql, mixed... $params){
	
		return $this->preparedQueryFetch($strSql, PDO::FETCH_ASSOC, ...$params);
	}
	
	public function  preparedQueryFetch($strSql, $fetchMode, mixed... $params){
	
		$arResult = null;
		
		try {
	
			if ( $this->connect() ){
				$this->con->query("SET NAMES 'utf8'");
				$stmt =  $this->con->prepare($strSql);
				$this->logStart($strSql, ...$params);
				if($stmt->execute($params)) {
					
					$stmt->setFetchMode($fetchMode);
	
					$arResult = $stmt->fetchAll();
					
				}
				$this->logEnd();
					
				$stmt->closeCursor();
					
			}
	
	
		}catch (Exception $e){
			getLogger()->error($e);
		}
	
		return $arResult;
	
	}
	
}



?>