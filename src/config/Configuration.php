<?php

function getConfig($key): mixed{

if (isset($GLOBALS['config'][$key])){
    return $GLOBALS['config'][$key];
}
return null;
}

(function() { 

    global $config; 

    $_environment = getenv('environment', true) ?: getenv('environment');
    $_configfile = getenv('conf_file', true) ?: getenv('conf_file');
    
        if(!$_environment){
            $_environment = 'dev';
        }

        if(!$_configfile){
            $_configfile = 'config.ini';
        }

        $basedir = __DIR__ . '/../../../../../config/';

        $configpath = $basedir . $_environment . '_' . $_configfile;

        if(file_exists($configpath)){
            $config = parse_ini_file($configpath);
        }else if( file_exists( $basedir . 'conf/config.ini') ){
            $config = parse_ini_file($basedir . 'config.ini');
        }else{
            $config = array();
        }

 })();


?>