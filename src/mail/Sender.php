<?php

namespace Mapeq\Mail;

use PHPMailer\PHPMailer\PHPMailer;

class Sender{

	function __construct($empty = false ){
        $this->init();
    }

    private PHPMailer $mail;
    private array $recipients = array();

    private String $header;
    private String $body;
    private String $footer;
    private String $subject;
    private String $disclaimer;
    private array $variables = array();

    public static String $TEXT_DISCLAIMER = "The content of this message is confidential. If you have received it by mistake, please inform us and then delete the message. It is forbidden to copy, forward, or in any way reveal the contents of this message to anyone. The integrity and security of this email cannot be guaranteed. Therefore, the sender will not be held liable for any damage caused by the message.<br>
    Warning: Although the company has taken reasonable precautions to ensure no viruses are present in this email, the company cannot accept responsibility for any loss or damage arising from the use of this email or attachments.<br>
    Please consider the environment before printing this email. Every unprinted email helps the environment.";

   private function init()  {
        $this->mail = new PHPMailer(true);

        $this->mail->isSMTP();
        $this->mail->SMTPAuth = true;
        // Persönliche Angaben

        $this->mail->Host = getConfig('MAIL_HOST');
        $this->mail->Port = getConfig('MAIL_PORT');
        $this->mail->Username = getConfig('MAIL_USER');
        $this->mail->Password = getConfig('MAIL_PW');
        $this->mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;

        $this->mail->CharSet = 'UTF-8';
    //  $this->mail->Encoding = 'base64';

        $this->mail->setFrom(getConfig('MAIL_SENDER'), getConfig('MAIL_SENDER_NAME'));
        $this->mail->isHTML(true);
    
   }

   public function  withParameter (String $name, string $value) :self{
     $this->variables['${'. $name. '}'] = $value;
     return $this;
   }

   public function  withFooter (String $footer) :self{
     $this->footer = $footer;
     return $this;
   }

   public function  withSubject (String $subject) :self{
     $this->subject = $subject;
     return $this;
   }

   public function  withDisclaimer (String $disclaimer) :self{
     $this->disclaimer = $disclaimer;
     return $this;
   }

   public function  withBody (String $body) :self{
     $this->body = $body;
     return $this;
   }

  public function withRecipient(string $recipient) : self{
     array_push($this->recipients , $recipient);
     return $this;
   }

   private function replace($text) : String {

     return str_replace(array_keys($this->variables), array_values($this->variables), $text);
   }


   public function send(string $subject = null, string $msg = null){

     // Empfänger, optional kann der Name mit angegeben werden
     foreach ($this->recipients as $rec) {
          $this->mail->addAddress($rec);
     }
     // Kopie
    // $this->mail->addCC('info@example.com');

     // Betreff
     if(isset ($subject)){
     $this->mail->Subject = $subject;
     }else{
          $this->mail->Subject = $this->subject;
     }

     $this->mail->Body = "";

     if(isset ($msg)){
          $this->mail->Body .= $msg;
     }else{

          if(isset($this->header)){
               $this->mail->Body .=  $this->header;
          }

          if(isset($this->body)){
               $this->mail->Body .=  $this->body;
          }

          if(isset($this->footer)){
               $this->mail->Body .= $this->footer;
          }
     }

     if(isset($this->disclaimer)){
          $this->mail->Body .= '<p style="font-size: 11px; color: #767272;">'. $this->disclaimer . '</p>';
     }

     $this->mail->Body = $this->replace($this->mail->Body);

     getLogger()->debug($this->mail->Body);

     $this->mail->send();

   }
    
}


?>