<?php

$basedir = __DIR__. '/../../../../../';


echo "###############################################\n
\tStart Init RestWS\n
###############################################\n\n";


echo "create log dir\n";

if (!file_exists($basedir."/logs")){
    mkdir($basedir."/logs") or die("Target dir could not be created");
}

echo "create config dir\n";

if (!file_exists($basedir."/config")){
    mkdir($basedir."/config") or die("Target dir could not be created");
}

$myfile = fopen($basedir. '/config/dev_config.ini', "w") or die("Unable to open file!");

    fwrite($myfile,"#Application\nAPP_NAME=ApiTestApp\n\n#Database\n#connection=\"mysql:host=0.0.0.0:3307;dbname=name;\"\n#username=\"user\"\n#passwort=\"password!\"\n#sequence=idprovider\n\n#LOGGER\nLOG_LEVEL=DEBUG\nSQL_LOGGER='TRUE'");

    fclose($myfile);

    echo "create public dir\n";

    if (!file_exists($basedir."/public")){
        mkdir($basedir."/public") or die("Target dir could not be created");
    }
    
    $myfile = fopen($basedir. '/public/.htaccess', "w") or die("Unable to open file!");
    
        fwrite($myfile,"# Enable the mod_rewrite engine\nRewriteEngine On\n\n# Exclude the /img/ path from redirection\nRewriteCond %{REQUEST_URI} !^/img/\n\n# Redirect all other requests to index.php\nRewriteRule ^(.*)$ /index.php [L,QSA]");
    
        fclose($myfile);

        $myfile = fopen($basedir. '/public/index.php', "w") or die("Unable to open file!");

        fwrite($myfile,"<?php\nuse Mapeq\RestWS\RestApi;\nrequire_once __DIR__ . '/../vendor/autoload.php';\n\n \$restApi =  new RestApi();\n\$restApi->doResponse();\n?>");


?>