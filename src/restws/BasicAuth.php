<?php

namespace Mapeq\RestWS;

use Mapeq\DB\Database;
use Mapeq\Mail\Sender;
use PDO;

class BasicAuth{

  const  MODE_ALPHANUM = 1;
  const  MODE_ALPHA = 2;
  const  MODE_NUM = 3;


  private array $required = array("password", "password2", "username");
  private array $unique = array("username");
  private bool $emailUniqe = false;
  private bool $emailConfirmation = false;
  private Sender $mailer;


  public function withRequire(string $field) : self{
      array_push($this->required, $field);
      return $this;
  }

  public function withEmailUnique() :self{
    $this->emailUniqe =true;
    return $this;
  }

  public function withConfirmation(Sender $mailer) : self{
    $this->emailConfirmation = true;
    $this->mailer = $mailer;
    return $this;
  }

   static private function userFromData($arData) : Context{
        $ctx = new Context();

        $ctx->username = $arData['username'];
        $ctx->firstname = $arData['firstname'];
        $ctx->lastname = $arData['lastname'];
        $ctx->email = $arData['email'];
        $ctx->apiKey = $arData['apiKey'];

        return $ctx;
    }


    function confirmUser(Database $db, $token): bool{

     $res = $db->preparedQuerySingleResult("select * from token where apikey = ?", $token);
     if(isset($res) ){
        $db->preparedInsert("update user set confirmed=true where id=? ;", $res['user_fk']);
        $db->preparedInsert("delete from token where id=?;", $res['id']);
        return true;
     }

     return false;

    }

/**
 * 
 */
    function registerUser(Database $db, array $arCtx) : Response{

    //$json = file_get_contents('php://input');
    //$ctx = json_decode($json);

    //$arCtx = (array) $ctx;
    
    $ctx = (Object) $arCtx;


    foreach ($this->required as $field) {
      if ( !isset($arCtx[$field])  || strlen($arCtx[$field]) === 0 ){
        return Response::Error(400 ,"MISSING_FIELD", "$field is missing");
      }
    }

      if($ctx->password != $ctx->password2){
        return Response::Error(400 ,"PWORD_IDENTITY", "Passwords do not match");
      }


      $password = $ctx->password;
     
      $salt = bin2hex(random_bytes(5));

      $pw = 'md5%'. $salt . '%' . md5($salt.$password);

      $username = isset($ctx->username) ? $ctx->username :null;
      $firstname = isset($ctx->firstname) ? $ctx->firstname :null;
      $lastname = isset($ctx->lastname) ? $ctx->lastname :null;
      $email = isset($ctx->email) ? $ctx->email :null;

      $res = $db->preparedQueryFetch('select 1 num from `user` u where  username = ?',
             PDO::FETCH_ASSOC , $username );

      if(count($res) > 0){
        return Response::Error(400 ,"USERNAME_USED", "Username is not available");
      }

      if($this->emailUniqe){ 
          $res = $db->preparedQueryFetch('select 1 num from `user` u  where  email = ?',
                 PDO::FETCH_ASSOC, $email);

          if(count($res) > 0){
            return Response::Error(400 ,"EMAIL_USED", "Email is not available");
          }
      }

      $con = $db->getCon();

      try {
      
      $con->beginTransaction();

    $userId =  $db->preparedInsert('insert into user 
      (username , password, firstname, lastname, email) 
      values (?,?,?,?,?)', array($username, $pw, $firstname, $lastname, $email ));

    $user =  $db->preparedQuerySingleResult('select firstname, lastname, id, username from user where id like ?', array($userId));


        if($this->emailConfirmation && isset($this->mailer)){
          $token = $this->createConfirmationToken($db, $user['id']);

          $this->mailer->withRecipient($email)
          ->withParameter('username', $username)
          ->withParameter('token', $token)
          ->withParameter('baseURL', RestApi::getBaseURL())
          ->send()
          ;

        }


      $response = new Response();
      $response->setContent($user);
      $response->withHeader('apikey', self::createApiKey($db, $user['id']));

      $con->commit();

    } catch (\Throwable $th) {
      getLogger()->error($th);
      $response =  Response::Error(400 ,"ERR", "");
    }

      return $response;

    }

    public static function resetPassword(){

    }

   static function loginUser(Database $db, string $user, string $pw) : Context{
        $ctx = new Context();

        if( isset($user) && isset($pw)){
    
              if ($user == 'apikey'){
    
                $result = $db->preparedQuery('select * 
                                    from user u
                                    join Token t on t.userFk = u.id 
                                    where t.apikey = ?',array($pw), PDO::FETCH_ASSOC);
                              
    
                  if (  count($result)  == 1){

                    $ctx = self::userFromData($result);
                  }
    
              }else{
    
                $result = $db->preparedQuery('select * 
                                          from v_login 
                                          where loginname = ?',array($user) , PDO::FETCH_ASSOC);
    
                    if (count($result) >= 1){

                      $data = $result[0];
                      $salt = $data['salt'];
                      $pwDB = $data['pword'];
                      $calcpw = md5($salt.$pw);
    
                        if( md5($salt.$pw) === $pwDB){
                          $ctx = self::userFromData($result);
                          $ctx->apiKey = bin2hex(random_bytes(32));
    
                        }
    
                    }                    
              }
    
          }
    
          return $ctx;
    }

    public static function createPasswordResetForm(Database $db, int $userId): Response {
      $resp = new Response();

      $strHTML = "<h1>User not found</h1>";

      try {
      
      $user = $db->preparedQuerySingleResult("select * from user where id = ?", array($userId));

      if(isset($user) ){ 

      $username = $user['username'];

      $token = isset($_GET['token']) ? $_GET['token'] : '';

      $strHTML = "<html><head><style>
                  .container {
                    display: flex;
                    justify-content: center;
                    align-items: center;
                    min-height: 100vh;
                    flex-direction: column;
                  }
                  </style></head><body>
                <div class=\"container\">
                <h1>Password Reset $username</h1>
                <form method=\"post\" action=\"/user/$userId/password\">
                    <label for=\"token\">Reset Code</label><br>
                    <input type=\"text\" id=\"token\" name=\"token\" value = \"$token\"><br>
                    <label for=\"pw1\">New Password</label><br>
                    <input type=\"password\" id=\"pw1\" name=\"password\"><br>
                    <label for=\"pw2\">Repeat New Password</label><br>
                    <input type=\"password\" id=\"pw2\" name=\"pw2\"><br><br>
                    <input type=\"submit\" value=\"Reset Password\">
                </form> </div> 
                </body></html>
    ";

      }

      } catch (\Throwable $th) {
        getLogger()->error($th);
      }

    $resp->setResponseMode(RestApi::RESPONSE_HTML);
        
    $resp->setContent($strHTML);
        return $resp;
    
    }

    public static function createApiKey(Database $db , int $userId) :string{
        return self::createToken($db, $userId, 'APIKEY');
    }

    public static function createConfirmationToken(Database $db , int $userId) :string{
      return self::createToken($db, $userId, 'CONFIRM', 5, self::MODE_NUM, '1 DAY' );
    }

    public static function createResetToken(Database $db , int $userId) :string{
      return self::createToken($db, $userId, 'RESET', 5);
    }

    public static function createToken( Database $db,  int $userId, string $code, int $length = 32, int $mode = self::MODE_ALPHANUM, string $valid = "5 DAY" )  : string{

      $apiKey = '';
      switch ($mode) {
        case 1:
          $apiKey = bin2hex(random_bytes($length));
          break;

        case 2:
          $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';  // All uppercase and lowercase letters
                for ($i = 0; $i < $length; $i++) {
                      $apiKey .= $characters[random_int(0, strlen($characters) - 1)];  // Select a random character from the alphabet
                }
          break;

        case 3:
          for ($i = 0; $i < $length; $i++) {
            
            $apiKey .= random_int(0, 9) + $i == 0 ? 1 :0;  // Append a random digit between 0 and 9
          }

          break;
        
      }

      $db->preparedInsert("  INSERT INTO token
			                      (user_fk, apikey, expire, code)
			                      VALUES(?, ?, INTERVAL ". $valid ." + now(),?);", array( $userId, $apiKey, $code));

        return $apiKey;
    }


    public function duration(){

    }


}
?>