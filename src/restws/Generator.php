<?php

$filename = isset($argv[1]) ? $argv[1] :  'api.json';
$foldername = isset($argv[2]) ? $argv[2] :  'api/';

$basedir = __DIR__. '/../../../../../';

$schema_file = $basedir . $filename;
$output_dir =  $basedir . $foldername;

echo "###############################################\n
\tStart API generation\n
###############################################\n\n";

echo "Import file : $schema_file\n";
echo "Target dir : $output_dir\n";

echo "-----------------------------------------------\n";


if (!file_exists($schema_file)){
    echo "File not found";
    die;
}

if (!file_exists($output_dir)){
    mkdir($output_dir) or die("Target dir could not be created");
}


$json = file_get_contents( $schema_file);

$api = json_decode($json,true);

$tags = array();
$routes = array();

foreach ($api['paths'] as $path => $resource) {

    echo "create resource $path\n";

    foreach ($resource as $method => $function) {
       // var_dump($function);

        $path_param = array();
        $description = '';
        if(array_key_exists('description', $function)){ 
            echo $function['description']."\n";
          //  $description = "/** \n *" . $function['description'] ."\n*/\n";
            $description = $function['description'] . "\n";

        }

        if(array_key_exists('parameters', $function)){ 
            foreach ($function['parameters'] as $param) {
                if($param['in'] == 'path'){ 
                    $path_param['{'. $param['name'] .'}'] = array(  $param['name'] =>  $param['schema']['type']);
                }
            }
        }

        $name = $method;
        $param = '';
        $paramlist = array();
        $route_path = $path;
        foreach (explode("/", $path) as $part  ){

           if(array_key_exists($part, $path_param)){ 
            
            $regex = ':.+';
            $type = 'string';
            foreach($path_param[$part] as $t){ 
                if($t == 'integer'){
                    $regex = ':\d+';
                    $type = 'int';
                }
            }

              $route_path =  str_replace( array_keys( $path_param[$part])[0], array_keys( $path_param[$part])[0] . $regex, $route_path);
                array_push( $paramlist, $type . ' $'.  array_keys( $path_param[$part])[0]);
            }else{
                $name = $name.  ucfirst( $part)  ;
            }
        }
        $param = '('. join(', ' ,   $paramlist) .')' ;
        $class = $function['tags'][0] . '::class';

        if( $method == 'get' && count($paramlist) == 0){
            $name = $name . 's';
        }

        $commment = "/**\n*" . $description ."*\n* ". $route_path ."\n*/\n";

        $interface =  $commment.  'public function '. $name.   $param.   ': Response;';

        if(key_exists($class , $tags)){
            array_push(  $tags[$class], $interface);
        }else{
            $tags[$class] = array($interface);
        }

        $method = strtoupper($method);

        $output =  " ->withFunction('$route_path', Api\\$class, '/$name', '$method')";

        array_push($routes, $output);

    }

}

foreach ($tags as $tag => $interfaces) {

    $filename = str_replace('::class', '', $tag );

    echo "write interface $filename\n";
    
    $myfile = fopen($output_dir.  'I' . $filename . '.php', "w") or die("Unable to open file!");
    $class_file = fopen($output_dir. $filename . '.php', "w") or die("Unable to open file!");


    fwrite($myfile, "<?php \n  namespace Api;\n use Mapeq\RestWS\Response; \n\n interface I$filename {\n\n");
    fwrite($class_file, "<?php \n  namespace Api;\n use Mapeq\RestWS\Response;\n use Mapeq\RestWS\Handler; \n\n class $filename extends Handler implements I$filename {\n\n");


    foreach ($interfaces as $inter ) {
        fwrite($myfile, "$inter \n\n");
        fwrite($class_file,  str_replace(';', " {\n\treturn new Response(true);\n}", $inter) ."\n\n");

    }

    fwrite($myfile, " }\n?>");
    fwrite($class_file, " }\n?>");


    fclose($myfile);
    fclose($class_file);

}
echo "write routes \n";

$myfile = fopen($output_dir. 'routes.txt', "w") or die("Unable to open file!");


foreach ($routes as $route ) {
    fwrite($myfile, "$route \n");
}

fclose($myfile);

echo "###############################################\n
Api generation succesful\n
" . count($routes) . " routes and " . count($tags) . " interfaces created";

?>