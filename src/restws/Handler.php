<?php

namespace Mapeq\RestWS;

use Mapeq\DB\Database;

abstract  class Handler {
	
	protected $status = 200;
	protected $contentType = 'text/javascript';
	protected $responseMode = RestApi::RESPONSE_JSON;
	protected Response $response;
	private Database $db;
	private Context $context;

	function __construct(Database $db, Context $context){
		$this->response = new Response();
		$this->db = $db;
		$this->context = $context;
	}
	
	function login(){
		return true;
	}
	

	function getContext(): Context{
			return $this->context;
	}

	function getDB() : Database {
		return $this->db;
	}

	protected function checkLogin($token){
		return true;
	}
	
	
	protected function returnNotLoggedIn(){
		$this->status = 403;
		$this->contentType = 'text/html';
	}
	
	function GET ($id) {
		$this->response->setStatus(405);
		return $this->response;
	}
	
	function POST ($ar_Params){
		$this->status = 405;
		return null;
	}
	
	function PUT ($ar_Params){
		$this->status = 405;
		return null;
	}
	
	function DELETE ($ar_Params){
		$this->status = 405;
		return null;
	}
	
	function OPTIONS ($ar_Params){
		$this->status = 405;
		return null;
	}
	
	function  getStatus(){
		return $this->status;
	}
	
	function  getContentType(){
		return $this->contentType;
	}
	
	function  getResponseMode(){
		return $this->responseMode;
	}

	function getRequestData() {

		$data = null;

		if(  isset($_POST) && count($_POST) > 0 ){
			$data = $_POST;
		}else{
			$input = file_get_contents('php://input');
		//	var_dump($input);	

			if(isset($input)){
				//parse_str($input, $data);
				$data = json_decode($input, true);
			}
		}

		return  $data;

	}
	
	function getRequestDataAsObject() : Object {
		return (Object) $this->getRequestData();
	}
	
	
}

?>