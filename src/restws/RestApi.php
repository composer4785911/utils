<?php

namespace Mapeq\RestWS;

use FastRoute;
use Mapeq\DB\Database;
use Mapeq\Logger\Watch;
use Monolog\Level;

$config; 

class RestApi {
	
	const  RESPONSE_JSON = 1;
	const  RESPONSE_RAW = 2;
	const  RESPONSE_IMAGE = 3;
	const  RESPONSE_HTML = 4;
	const  RESPONSE_SSE = 5;

	private $db;
	private Response $response;
	private $resourses;
	private $config;
	private  $auth_handler;
	private  $context;
	
	function __construct(){
		$this->response = new Response();
		$this->resourses = array();
		$this->db = new Database;
		$this->context = new Context();
	}

	public function doResponse(){
		ob_start();
		$watch = new Watch(Level::Debug);

			try {
				$watch->begin();
					$this->getResource();
				$watch->stop("getResource()");

				$watch->begin();
					$this->doRequest();
				$watch->stop("doRequest()");

			} catch (\Throwable $th) {
				getLogger()->error($th);
				http_response_code(500);
			}
		
		$i = ob_get_clean();
		echo trim($i);
	}

	private function createContext(){
		try {
			
			if( (  (  isset($_SERVER['PHP_AUTH_USER']) && isset($_SERVER['PHP_AUTH_PW']) ) || isset($_GET["apikey"]) ) &&
						isset($this->auth_handler) ){

					$usr = $_SERVER['PHP_AUTH_USER'];
					$pw = $_SERVER['PHP_AUTH_PW'];

					if(isset($_GET["apikey"])){
						$usr = 'apikey';
						$pw = $_GET["apikey"];
					}

				$this->context = call_user_func($this->auth_handler, $this->db, $usr, $pw);
			}
		} catch (\Throwable $th) {}
	}

	public function withResource(string $path, mixed $class, string ...$methods) : RestApi{
		 if(count($methods) == 0){
			$methods = array('GET', 'POST', 'PUT', 'DELETE' );
		 }
		  array_push($this->resourses, array("path" => $path, "class" => $class, "function"=> null, "methods"=> $methods));
		  array_push($this->resourses, array("path" => $path .'/{id:\d+}', "class" => $class, "function"=> null, "methods"=> $methods));


		return $this;
	}

	public function withFunction(string $path, mixed $class, string $function, string $method, array $params = array()) : self{
		$methods = array();
		if(!isset($method)){
		   $methods = array('GET', 'POST', 'PUT', 'DELETE' );
		}else{
			$methods = array($method);
		}
		 array_push($this->resourses, array("path" => $path, "class" => $class, "function"=> $function, "methods"=> $methods, "params" => $params));

	   return $this;
   }

   public function withSwaggerUI(string $path = '/swagger') :self{
		array_push($this->resourses, array("path" => $path, "class" => Swagger::class, "function"=> '/swaggerUI', "methods"=> array('GET')));
		array_push($this->resourses, array("path" => $path. '/json', "class" => Swagger::class, "function"=> '/getJson', "methods"=> array('GET')));
   		return $this;
	}

	public function withBasicAuthHandler() : self {

		$this->auth_handler =	function (Database $db, string $user, string $pw):Context {
			return BasicAuth::loginUser( $db,  $user,  $pw);
		};

		return $this;
	}

	public function withAuthHandler(callable $handler) : self {
		$this->auth_handler = $handler;

		return $this;
	}

	
	private function getResource(){
		$watch = new Watch(Level::Debug);

		$watch->begin();
		$dispatcher = FastRoute\simpleDispatcher(function(FastRoute\RouteCollector $r) {

			foreach ($this->resourses as $res) {


				 $params = array_key_exists('params', $res) ? $res['params'] : array("foo"=> 'bar');
				
				if($res['function'] != null  ){

					$r->addRoute($res['methods'][0], $res['path'], $res['class'] . $res['function'] , $params  );

				}else{ 

					foreach ($res['methods'] as $m) {

						$function = '/'.strtolower($m);

						$r->addRoute($m, $res['path'], $res['class'] . $function,  $params  );
					}

				}
			}
		});
		$watch->stop("prepare dispatcher");
		
		
		$watch->begin();
		
		// Fetch method and URI from somewhere
		$httpMethod = $_SERVER['REQUEST_METHOD'];
		$uri = $_SERVER['REQUEST_URI'];
		
		if (false !== $pos = strpos($uri, '?')) {
			$uri = substr($uri, 0, $pos);
		}
		$uri = rawurldecode($uri);
		
		getLogger()->debug('Request: ' . $httpMethod . ' ' . $uri);

		$routeInfo = $dispatcher->dispatch($httpMethod, $uri);

		$watch->stop("dispatch");

		switch ($routeInfo[0]) {
			case FastRoute\Dispatcher::NOT_FOUND:
				// ... 404 Not Found
				$this->response->setContent('Not Found');
				$this->response->setStatus(404);
				break;
			case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
				$allowedMethods = $routeInfo[1];
				// ... 405 Method Not Allowed
				if($httpMethod == 'OPTIONS'){
					$this->response->setStatus(200);
				}else{
					$this->response->setContent('405 - Method Not Allowed');
					$this->response->setStatus(405);
				}
				break;
			case FastRoute\Dispatcher::FOUND:
				$handler = $routeInfo[1];
				$vars = $routeInfo[2];

				$params =  $this->getParameter($routeInfo);

				if($params["AUTH"] === true){
					$watch->begin();
						$this->createContext();
					$watch->stop("createContext()");
				}

				// ... call $handler with $vars
				list($class, $method) = explode("/", $handler, 2);

				$this->response = call_user_func_array(array(new $class($this->db, $this->context), $method), $vars);
				break;
		}
						
	}
	

	private function getParameter(mixed $routeInfo) : array {


		$params = array("AUTH" => true);

		try {
			$extra = $routeInfo->extraParameters;

			if( array_key_exists("AUTH",  $extra )){
					if(is_bool($extra["AUTH"])){
						$params["AUTH"] = $extra["AUTH"];
					}
			}

		} catch (\Throwable $th) {

		}

		return $params;
		
	}

  protected function checkLogin(){
  	return true;
  }

  
  private function getMethod(){
  	$method = $_SERVER['REQUEST_METHOD'] ;
  	
  	if($method === 'POST'){
  		if (isset( $_REQUEST['METHOD'])){
  			$method = $_REQUEST['METHOD'];
  		}
  	}
  	
  	return $method;
  }

   private function outputRAW(){
   	
   	echo $this->response->getContent();
   }
	
	private function outputJSON () {

		if(isset($this->response)){
			$this->response->setContentType('application/json');
	
			$content = $this->response->getContent();
			if($this->response->isSingleResult() ){
				if(is_array($content) && count($content) == 0 ){
					$this->response->setStatus(404);
					$content = array("status" => 404, "message" => "not found");
				}else{
					$content = $content[0];
				}
			}
			$content = json_encode($content);


			if (isset($_REQUEST['callback']) ) {
				$content = $_REQUEST['callback'] . '('  . $content . ');';
			}
			echo $content;
		//	$this->response->setContent(json_encode($content));
		}

		

	}
	
	private function outputImage(){
		$image = $this->response->getContent();

		$this->response->setContentType('image/jpeg');

		getLogger()->debug('try find image ' . $image);
			// Path to the image
		//	$imagePath = 'path/to/your/image.jpg';

			// Check if the image exists
			if (file_exists($image)) {
				// Read and output the image
				readfile($image);
			} else {
				// Return an error image if the file doesn't exist
				echo 'Image not found!';
			}


		/*
		imagejpeg($image);
		
  		imagejpeg( $image, NULL, 100 ); 
  		imagedestroy( $image ); 
  		*/
	}

	private function outputHTML(){
		$this->response->setContentType('text/html');
		echo $this->response->getContent();
	}

	private function outputSSE(){
	
		date_default_timezone_set("Europe/Berlin");

		header('Content-Type: text/event-stream');
		header('X-Accel-Buffering: no');


		set_time_limit(1800);

		$loop = 0;
		$last = null;
		while (true) {
			// Every second, send a "ping" event.
		  
			if($this->response->getPing() > 0 && $loop % 10 == 0){ 
				echo "event: ping\n";
				$curDate = date(DATE_ATOM);
				echo 'data: {"ping": "' . $curDate . '"}';
				echo "\n\n";
			}
		  
			// Send a simple message at random intervals.
		  		  
			$payload = call_user_func($this->response->getContent(), $last);

			if(isset($payload)){

				$data = 'data: ' . $payload . "\n\n";
				$last = $payload;
			  	echo $data;
			}

			if (ob_get_contents()) {
				ob_end_flush();
			}
			flush();
		  
			// Break the loop if the client aborted the connection (closed the page)
		  
			if (connection_aborted()) break;
		  
			$loop++;
			usleep($this->response->getRefresh());
		}

	}


	private function doRequest(){

		header('Cache-Control: no-cache');
		header('Pragma: no-cache');
		header('Access-Control-Allow-Origin: *');
	//	header('Access-Control-Allow-Headers: X-Requested-With');
		header("Access-Control-Allow-Credentials: true");
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, Content-Range, Content-Disposition, Content-Description');

		foreach ($this->response->getHeaders() as $header => $val)  {
				header("$header: $val");
		}
		
		if($this->response->getStatus() >= 300){
			$this->response->setContentType('text/plain');
		}

		switch ($this->response->getResponseMode()) {
			case  self::RESPONSE_JSON :
					$this->outputJSON();
				break;
			
			case self::RESPONSE_RAW :
				echo $this->response->getContent();
				break;
				
			case self::RESPONSE_IMAGE :
					$this->outputImage();
				break;

			case self::RESPONSE_HTML :
					$this->outputHTML();
				break;

			case self::RESPONSE_SSE :
					$this->outputSSE();
				break;
			
		}

		header('Content-Type: '. $this->response->getContentType());
		http_response_code($this->response->getStatus());


	}


	public static function getBaseURL() : string{
		$protocol = isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? 'https' : 'http';

		// Get the domain name (host)
		$host = $_SERVER['HTTP_HOST'];  // This includes the domain and possibly the port (e.g., 'example.com' or 'example.com:8080')

		// Get the request URI (includes path and query string)
		$requestUri = $_SERVER['REQUEST_URI'];  // This includes the path and query string

		// Build the full URL
		$url = $protocol . "://" . $host ;

		return $url;
	}
	

}


?>