<?php

namespace Mapeq\RestWS;

use Mapeq\DB\Database;
use PDO;

class QueryResponse extends Response{

    function __construct(Database $db, string $query, array $param = null ){
        parent::__construct(false);

        $rs = null;
        if(isset($param)){
            $rs = $db->preparedQueryFetch($query, PDO::FETCH_ASSOC, $param);
        }else{
            $rs =  $db->simpleQueryFetch($query, PDO::FETCH_ASSOC);
        }

        $this->setContent($rs);

    }

}

?>