<?php

namespace Mapeq\RestWS;


class Response{
	
	const  RESPONSE_JSON = 1;
	const  RESPONSE_RAW = 2;
	
	private  $content ;
	private $status;
	private $contentType;
	private $responseMode;
	private bool $singleResult;
	private $header = array();
	private int $ping = 0;
	private int $refreshInterval = 1000000;
	
	
	function __construct($empty = false ){
		
		$this->status = 200;
		$this->contentType = 'text/javascript';
		$this->responseMode = self::RESPONSE_JSON;
		$this->singleResult = false;

		if($empty){ 
			$this->status = 501;
			$this->contentType = 'text/plain';
			$this->responseMode = self::RESPONSE_RAW;
			$this->singleResult = false;
			$this->content = "Not implemented";
		}
	}


	static function Error (int $status, string $code, string $msg): self{
		$content = array();
		$content['code'] = $code;
		$content['message'] = $msg;
		
		$err = new Response();
		$err->status = $status;
		$err->content= $content;
		

		return $err;
	}

	function getHeaders() {
		return $this->header;
	}

	function withHeader($key, $value) :self{
		$this->header[$key] = $value;
		return $this;
	}
	
	
	function  getContent(){
		return $this->content;
	}
	
	function setContent($content){
		$this->content = $content;
	}

	function  getPing() : int{
		return $this->ping;
	}
	
	function setPing( int $ping ){
		$this->ping = $ping;
	}

	function setRefresh(int $refresh)  {
		$this->refreshInterval = $refresh * 1000000;
	}

	function setRefreshMicro(int $refresh)  {
		$this->refreshInterval = $refresh;
	}

	function getRefresh(){
		return $this->refreshInterval;
	}

	function setContentSingleResult($content){

		if(is_array($content)){
			$this->content = $content[0];
		}

	//	$this->content = $content;
	}
	
	public function isSingleResult() : bool{
		return $this->singleResult;
	}

	public function  setSingleResult(bool $singleResult){
		$this->singleResult = $singleResult;
	}
	
	function  getStatus(){
		return $this->status;
	}
	
	function setStatus($status){
		$this->status = $status;
	}
	
	function  getContentType(){
		return $this->contentType;
	}
	
	function setContentType($contentTyp){
		$this->contentType = $contentTyp;
	}
	
	
	function  getResponseMode(){
		return $this->responseMode;
	}
	
	function setResponseMode($responseMode){
		$this->responseMode = $responseMode;
	}
	
	
}


?>