<?php

namespace Mapeq\RestWS;

class Context{

    public bool $logedin = false;
    public ?string $username = '';
    public ?string $firstname;
    public ?string $lastname;
    public ?string $apiKey = '';
    public ?string $password;
    public ?string $apiKeyRestore = '';
    public ?string $email;
    public array $roles = array();


    public function hasRole( bool $any = true, string ...$role) :bool{

        foreach ($role as $r) {
            if(in_array($r, $this->roles)){
                if($any){
                    return true;
                }
            }else if (!$any){ 
                return false;
            }
        }

        return false;

    }

}

?>