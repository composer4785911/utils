<?php

use Mapeq\RestWS\Response;
use Mapeq\RestWS\RestApi;

    class Examples{

        public function getUser(int $userId): Response {
            $resp = new Response();
        
            $resp->setResponseMode(RestApi::RESPONSE_SSE);
        
            $resp->setPing(true);
        
            $resp->setContent(
                function($last = null){
        
                    $db =$this->getDB();
                    $rs = $db->preparedQuerySingleResult("select * from usergroup");
        
                    $db->close();
        
                    $ret = json_encode($rs);
        
                    if( isset($last) && $ret == $last){
                        $ret = null;
                    }
        
                    return $ret;
        
                }
            );
    }

}


?>