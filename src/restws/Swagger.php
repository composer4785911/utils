<?php


namespace Mapeq\RestWS;

class Swagger{

    public function swaggerUI(){

        $json_path = '/swagger/json';
        include('SwaggerUI.inc.php');
        exit(0);
    }

    public function getJson(){

        $schema_file = __DIR__. '/../../../../../' . 'api.json';
       echo file_get_contents($schema_file);
        exit(0);
    }

    public static function getVersion() : string{

        $schema_file = __DIR__. '/../../../../../' . 'api.json';
        $json = file_get_contents( $schema_file);
        $api = json_decode($json,true);

        return $api['info']['version'];

    }

}

//$swagger =new Swagger();
//$swagger->swaggerUI();

?>