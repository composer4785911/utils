<?php

use Monolog\Handler\BrowserConsoleHandler;
use Monolog\Level;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;


function getLogger() : Logger {

    if (isset($GLOBALS['logger']) ){
        return $GLOBALS['logger'];
    }else{
        return new Logger('');
    }
    return null;
}


(function() { 
    global $logger;


$logfile =  getConfig('LOG_FILE');
$logpath =  getConfig('LOG_PATH');
$level =    getConfig('LOG_LEVEL');

$loglevel = null;
switch ($level) {
    case 'INFO':
        $loglevel = Level::Info;
        break;

    case 'DEBUG':
        $loglevel = Level::Debug;
        break;

    case 'WARN':
        $loglevel = Level::Warning;
        break;
    
    case 'ERROR':
        $loglevel = Level::Error;
        break;

    case 'SEVERE':
        $loglevel = Level::Critical;
        break;

    case 'OFF':
        $loglevel = null;
        return;
        break;
    
    default:
        $loglevel = Level::Info;
        break;
}


$logfile = !isset($logfile) ? 'test.log' : $logfile;

$logdir =  !isset($logpath) ?  __DIR__ . '/../../../../../logs/' . $logfile :  $logpath;

if( isset($loglevel) ){

    $logger = new Logger('LOG');

    $logger->setTimezone(new DateTimeZone('Europe/Berlin'));

    if(getConfig('FORCE_TEXT_OUTPUT')=== 'TRUE'){
       register_shutdown_function('forceText');
    }
   
    if( null !== getLogger('LOG_FILE') ){ 
        $logger->pushHandler(new StreamHandler($logdir, $loglevel));
    }

    if(getConfig('LOGGER_BROSWER') === 'TRUE'){ 
       $logger->pushHandler(new StreamHandler('php://output', $loglevel));
    }

    if(getConfig('LOGGER_CONSOLE') === 'TRUE'){ 
        $logger->pushHandler(new BrowserConsoleHandler($loglevel)); 
    }
}

getLogger()->log(Level::Debug, 'Logger initialized');

})();


function forceText(){
    header('content-type: text/html');
   //header('content-type: text/plain');
}

?>