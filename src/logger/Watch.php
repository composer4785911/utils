<?php

namespace Mapeq\Logger;

use Monolog\Level;


class Watch {

    private float $start;
    private Level $loglevel;

    public function __construct(Level $loglevel) {
        $this->loglevel = $loglevel;
    }

    function begin($text = null, $level = null){
        if(!isset($level)){
            $level = $this->loglevel;
        }

        if(isset($text)){
            getLogger()->log($level, $text);
        }
      
      $this->start = floor(microtime(true) * 1000);
    }

    function stop($text = null, $level = null){
            if(!isset($level)){
                $level = $this->loglevel;
            }

            $duration = floor(microtime(true) * 1000) - $this->start;
            getLogger()->log($level, $text . " duration: $duration ms");
    }

}


?>