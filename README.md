# Utils


# Table of Contents
- [Installation](#Install)
- [Config](#Config)
- [Logger](#Logger)
- [Database](#Database)
- [RestWS](#RestWS)

## Install

To install with composer:

```sh
composer init
composer require mapeq/Utils
```
## Config

Reads configurations from config file. The default path is /config/<mark>environment</mark>_config.ini
The environment can be set as environment variable:

.htaccess
```
    <Files .htaccess>
        order allow,deny
        deny from all
    </Files>

    <IfModule mod_env.c>
        SetEnv APP_ENV 'production'
    </IfModule>
``` 

Basic usage
```php
$value = getConfig('key');
bool $boolvalue = getConfigBool('key');
```


| Key      | Description | Example |
| ----------- | ----------- | ----------- |
| connection| Database connection string | mysql:host=localhost:3307;dbname=example; |
| username| Database user |  |
| password| Database password |  |
| LOG_LEVEL| Sets the loglevel Defautl = ERROR | DEBUG |
| LOGGER_CONSOLE| Enables console output | TRUE |
| LOGGER_BROSWER| Enables browser output| TRUE |
| SQL_LOGGER| Enables SQL outpuloggingt | TRUE |
| LOG_FILE| Sets log file relative to project root | test.log |
| FORCE_TEXT_OUTPUT| Overides content type to text/html | TRUE |


## Logger

## Database

## RestWS

### Api Generation

Generate API from OA3 file:

```sh
php -f .\vendor\mapeq\utils\src\restws\Init.php  api.json api/

php -f .\vendor\mapeq\utils\src\restws\Generator.php  api.json api/
```

### General usage

```php
    $api = new RestApi();

    $api
    ->withFunction('/version', Comptest\General::class, '/getVersions', 'GET') 
    ->withSwaggerUI()
    ->withAuthHandler(
    function (Database $db, string $user, string $pw):Context {
        $ctx = new Context();

        if($pw == 'geheim'){
            $ctx->username = 'username';
            $ctx->logedin = true;

        }
        return $ctx;
    });


$api->doResponse();
```


